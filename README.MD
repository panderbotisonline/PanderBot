![PanderPanda](/data/PanderPanda.png?raw=true)




###########################################################
###########################################################
#########                PANDERBOT                 ########
#########   remove pandering using Ublock Origin   ########
###########################################################
###########################################################



#What is panderbot?

Panderbot's primary goal is to be a user submitted filter list  made to
provide people ease of mind from brands pushing an agenda on you or trying
to sell you something based off of your personality or personal attributes.
Think of it like sponsorblock for youtube, but more universal.



#Why did you create panderbot?

The idea came to fruition after the 2022 Pride month.  I noticed that not
only were people opposed to the month getting annoyed by corporate shilling
but also people in the LGBTQ+.  The more I thought about a solution the more
I was reminded of how this happens year round (e.g. St. Patrick's day,
Christmas, Black history month, etc.) and how nice it'd be to purchase things
without malicious marketing tactics.




#Installation

1.Open up pander_list.txt with the text editor of your choice

2.Click on the ublock origin icon on the browser of your choice

3.Click on the cog wheel on the bottom right

4.Click my filters

5.Go to your text file ctrl-a and ctrl-c or select all and copy

6.ctrl-v or paste the contents to the bottom line of your document

7.click apply changes on top right

DONE!





#CONTRIBUTION

This is perhaps the most important part of this document as the list cannot
be created without YOU.  All you need to do is submit a commit taking filters. 
you added.  If you're new to UBlock origin here is how you select an element

1.Open ublock origin and click on element picker on the bottom left

2.Once you select the element you want to remove it will show a text box with
what you are about to remove (in html). Copy and paste that into your commit

##EVERYTHING AFTER HERE IS IF ITS MORE THAN ONE ELEMENT##

3.If there is more than one element keep clicking create until all of that
element is removed.

4.Go into my filters (re-follow 2-4 in installation)

5.Copy all the elements you selected into your commit.

#LICENSE

Panderbot follows GPL 3.0, as I need to touch grass sometimes.  Inevitably
I will not be able to manage all the commits as I got real life to deal with.
As I want this to continue on past my own personal maintaining with whoever
wants to fork this.
